package business.handlers;

import beans.ReserveBean;
import business.Flight;
import business.Reservation;
import business.SeatClass;
import connection.ConnectionHandler;
import repository.FlightRepo;
import repository.ReserveRepo;
import repository.SeatClassRepo;

import java.util.Map;

/**
 * Created by Moein Sorkhei on 3/9/2017.
 */
public class ReserveHandler {
    public void reserve(Reservation res) {
        ReserveBean resBean = new ReserveBean();
        resBean.setParameters(res);
        ConnectionHandler connectionHandler = new ConnectionHandler();

        try {
            connectionHandler.reserveAndFinalize(resBean, res);
        } catch (IllegalArgumentException ex) {
            System.out.println("========= RESERVE NOT COMPLETED ");
            ex.printStackTrace();
        }

        Flight flight = FlightRepo.getInstance().getFlightIfExists(res.getFlight());
        if (flight == null) {
            flight = res.getFlight();
            FlightRepo.getInstance().addToRepo(res.getFlight());
            System.out.println("============ NEW FLIGHT ADDED TO REPO");
        }
        else {
            res.setFlight(flight);
        }

        for (Map.Entry<SeatClass, Integer> entry : flight.getSeats().entrySet()) {
            SeatClass flightSeatClass = entry.getKey();
            SeatClass temp = SeatClassRepo.getInstance().getSeatClassIfExists(flightSeatClass);
            if (temp == null) {
//                temp = flightSeatClass;
                SeatClassRepo.getInstance().addToRepo(flightSeatClass);
                System.out.println("============ NEW SEAT CLASS ADDED TO REPO");
            }
            else {
                flight.getSeatClassByName(temp.getClassOfSeat()).setId(temp.getId());
            }
        }

        SeatClass seatClass = SeatClassRepo.getInstance().getSeatClassIfExists(res.getSeatClass());
        if (seatClass == null) {
            SeatClassRepo.getInstance().addToRepo(res.getSeatClass());
            System.out.println("============ NEW SEAT CLASS ADDED TO REPO");
        }
        else {
            res.setSeatClass(seatClass);
        }

        ReserveRepo.getInstance().addToRepo(res);
        System.out.println("============ NEW RES ADDED TO REPO");
        System.out.println("RESERVE SET COMPLETELY\n");
        System.out.println(res.toString());
    }
}
