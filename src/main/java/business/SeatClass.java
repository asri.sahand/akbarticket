package business;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class SeatClass {
    private String originCode;
    private String destCode;
    private String airlineCode;
    private String classOfSeat;
    private int adultPrice;
    private int childPrice;
    private int infantPrice;
    private int id;

    public SeatClass(String originCode, String destCode, String airlineCode, String classOfSeat) {
        this.originCode = originCode;
        this.destCode = destCode;
        this.airlineCode = airlineCode;
        this.classOfSeat = classOfSeat;
        id = -1;
    }

    public SeatClass(String originCode, String destCode, String airlineCode, String classOfSeat,
                     int adultPrice, int childPrice, int infantPrice) {
        this.originCode = originCode;
        this.destCode = destCode;
        this.airlineCode = airlineCode;
        this.classOfSeat = classOfSeat;
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.infantPrice = infantPrice;
    }

    @Override
    public String toString() {
        return "SeatClass{" +
                "originCode='" + originCode + '\'' +
                ", destCode='" + destCode + '\'' +
                ", airlineCode='" + airlineCode + '\'' +
                ", classOfSeat='" + classOfSeat + '\'' +
                ", adultPrice=" + adultPrice +
                ", childPrice=" + childPrice +
                ", infantPrice=" + infantPrice +
                ", id=" + id +
                '}' + "\n\n";
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public void setInfantPrice(int infantPrice) {
        this.infantPrice = infantPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginCode() {
        return originCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getClassOfSeat() {
        return classOfSeat;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public int getInfantPrice() {
        return infantPrice;
    }
}
