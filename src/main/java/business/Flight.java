package business;

import java.util.Map;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class Flight {
    private String originCode;
    private String destCode;
    private String airlineCode;
    private String flightNo;
    private String date;
    private String departTime;
    private String arrivalTime;
    private String airplaneModel;
    private Map<SeatClass, Integer> seats;
    private int flightId;

    public Flight(String originCode, String destCode, String airlineCode, String flightNo, String date,
                  String departTime, String arrivalTime, String airplaneModel, Map<SeatClass, Integer> seats) {
        this.originCode = originCode;
        this.destCode = destCode;
        this.airlineCode = airlineCode;
        this.flightNo = flightNo;
        this.date = date;
        this.departTime = departTime;
        this.arrivalTime = arrivalTime;
        this.airplaneModel = airplaneModel;
        this.seats = seats;
        this.flightId = -1;
    }

    public SeatClass getSeatClassByName(String classOfSeat) {
        for (Map.Entry<SeatClass, Integer> entry: seats.entrySet()) {
            SeatClass seatClass = entry.getKey();
            if (seatClass.getClassOfSeat().equals(classOfSeat))
                return seatClass;
//            else
//                System.out.println(seatClass.getClassOfSeat() + " != " + classOfSeat);
        }
        return null;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getFlightId() {
        return flightId;
    }

    public String getOriginCode() {
        return originCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public String getDate() {
        return date;
    }

    public String getDepartTime() {
        return departTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public String getAirplaneModel() {
        return airplaneModel;
    }

    public Map<SeatClass, Integer> getSeats() {
        return seats;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "originCode='" + originCode + '\'' +
                ", destCode='" + destCode + '\'' +
                ", airlineCode='" + airlineCode + '\'' +
                ", flightNo='" + flightNo + '\'' +
                ", date='" + date + '\'' +
                ", departTime='" + departTime + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", airplaneModel='" + airplaneModel + '\'' +
                ", seats=" + seats +
                ", flightId=" + flightId +
                '}' + "\n\n";
    }
}
