package business;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class Ticket {
    private String ticketNo;
    private Passenger passenger;

    public Ticket(String ticketNo, Passenger passenger) {
        this.ticketNo = ticketNo;
        this.passenger = passenger;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketNo='" + ticketNo + '\'' +
                ", passenger=" + passenger.toString() +
                '}' + "\n\n";
    }
}
