package business;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class Reservation {
    private int adultCount;
    private int childCount;
    private int infantCount;
    private int adultPrice;
    private int childPrice;
    private int infantPrice;
    private SeatClass seatClass;
    private String token;
    private String refCode;
    private Flight flight;
    private List<Ticket> tickets;
    private int id;

    public Reservation(int adultCount, int childCount, int infantCount, Flight flight, SeatClass seatClass) {
        this.adultCount = adultCount;
        this.childCount = childCount;
        this.infantCount = infantCount;
        tickets = new ArrayList<Ticket>();
//        this.adultPrice = adultPrice;
//        this.childPrice = childPrice;
//        this.infantPrice = infantPrice;
        this.seatClass = seatClass;
//        this.token = token;
        this.flight = flight;
        id = -1;
    }

    public void setSeatClass(SeatClass seatClass) {
        this.seatClass = seatClass;
    }

    public int getTotalPrice() {
        return adultPrice + childPrice + infantPrice;
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setAdultPrice(int adultPrice) {
        this.adultPrice = adultPrice;
    }

    public void setChildPrice(int childPrice) {
        this.childPrice = childPrice;
    }

    public void setInfantPrice(int infantPrice) {
        this.infantPrice = infantPrice;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public int getChildCount() {
        return childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public int getInfantPrice() {
        return infantPrice;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public String getToken() {
        return token;
    }

    public String getRefCode() {
        return refCode;
    }

    public Flight getFlight() {
        return flight;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "adultCount=" + adultCount + '\n' +
                ", childCount=" + childCount + '\n' +
                ", infantCount=" + infantCount + '\n' +
                ", adultPrice=" + adultPrice + '\n' +
                ", childPrice=" + childPrice + '\n' +
                ", infantPrice=" + infantPrice + '\n' +
                ", seatClass=" + seatClass + '\n' +
                ", token='" + token + '\'' + '\n' +
                ", refCode='" + refCode + '\'' + '\n' +
                ", flight=" + flight.toString() +
                ", tickets=" + tickets + '\n' +
                ", id=" + id + '\n' +
                '}' + "\n\n";
    }
}