package business;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class Passenger {
    private String name;
    private String sname;
    private String SID;
    private String ageType;
    private String gender;

    public Passenger(String name, String sname, String SID, String ageType, String gender) {
        this.name = name;
        this.sname = sname;
        this.SID = SID;
        this.ageType = ageType;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getAgeType() {
        return ageType;
    }

    public String getName() {
        return name;
    }

    public String getSname() {
        return sname;
    }

    public String getSID() {
        return SID;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "name='" + name + '\'' +
                ", sname='" + sname + '\'' +
                ", sid='" + SID + '\'' +
                '}' + "\n\n";
    }
}
