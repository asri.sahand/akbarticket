package connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Moein Sorkhei on 2/8/2017.
 */

public class ServerConnection {
    private Socket socket;

    public ServerConnection(String serverName, int port) {
        boolean connectionTryAgain = false;
        while (true) {
            try {
                socket = new Socket(serverName, port);
                System.out.println("CONNECTION ESTABLISHED WITH REMOTE SERVER...");
                break;
            } catch (IOException e) {
                if (!connectionTryAgain) {
                    System.out.println("IO EXCEPTION WHILE ESTABLISHING CONNECTION WITH REMOTE SERVER... TRYING AGAIN");
                    connectionTryAgain = true;
                }
            }
        }
    }

    public String getResponse(String request) {
        System.out.println("\n\n-----------------------");
        System.out.println("========= in Connection: ");
        System.out.println(request);
        System.out.println("-----------------------\n\n");



        PrintWriter writer;
        BufferedReader reader;

        try {
            writer = new PrintWriter(socket.getOutputStream(), true);
            for (String line : request.split("\n")) {
//                System.out.println("SERVER CONNECT:");
//                System.out.println("SENT: " + line);
                writer.println(line);
            }
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println("PrintWriter/BufferedReader couldn't work in getResponse");
            e.printStackTrace();
            return "";
        }

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println("BufferedReader couldn't be generated in getResponse");
            e.printStackTrace();
        }

        String line, response = "";
        try {
            while ((line = reader.readLine()) != null) {
                response += (line + '\n');
                if (!reader.ready()) {
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("BufferedReader.ready() couldn't read in getResponse");
            e.printStackTrace();
            return "";
        }
//        if (response.length() == 1)
//            return "";
        if (response.length() > 0)
            response = response.substring(0, response.length() - 1);
//        else
//            System.out.println("RESP LEN: " + response.length());

        System.out.println("\n\n-----------------------");
        System.out.println("========= in Connection: ");
        System.out.println(response);
        System.out.println("-----------------------\n\n");

        return response;
    }

//    public String requestForFlights(String originCode, String destCode, String date) throws IOException {
//        String query = "AV " + originCode + " " + destCode + " " + date;
//        return getResponse(query);
//    }
//
//    public String requestPrice(String originCode, String destCode, String arlineCode, String seatClass) throws IOException {
//        String query = "PRICE " + originCode + " " + destCode + " " + arlineCode + " " + seatClass;
//        return getResponse(query);
//    }
//
//    public String requestForReservation(String reserveInfo) throws IOException {
//        String query = "RES " + reserveInfo;
//        return getResponse(query);
//    }
//
//    public String finalize(String token) throws IOException {
//        String query = "FIN " + token;
//        return getResponse(query);
//    }

//    public void closeResources() throws IOException {
//        socket.close();
//        reader.close();
//        writer.close();
//    }
}