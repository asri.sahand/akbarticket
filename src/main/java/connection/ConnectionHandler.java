package connection;

import beans.FinalizeBean;
import beans.PriceBean;
import beans.ReserveBean;
import beans.SearchBean;
import business.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */

public class ConnectionHandler {
    private ServerConnection serverConnection;
    private final Logger LOGGER = Logger.getLogger(ConnectionHandler.class.toString());
    public ConnectionHandler() {
        serverConnection = new ServerConnection("178.62.207.47", 8081);
    }

    public void reserveAndFinalize(ReserveBean resBean, Reservation reservation) throws IllegalArgumentException {
        String query = "RES " + resBean.getFlight().getOriginCode() + " " + resBean.getFlight().getDestCode() + " " +
                resBean.getFlight().getDate() + " " + resBean.getFlight().getAirlineCode() + " " +
                resBean.getFlight().getFlightNo() + " " + resBean.getSeatClass().getClassOfSeat() + " " +
                resBean.getAdultCount() + " " + resBean.getChildCount() + " " + resBean.getInfantCount() + '\n';
        for (Passenger passenger : resBean.getPassengers()) {
            query += passenger.getName() + " " + passenger.getSname() + " " + passenger.getSID();
            if (resBean.getPassengers().indexOf(passenger) != resBean.getPassengers().size() - 1)
                query += '\n';
        }

        String response = serverConnection.getResponse(query);
        if (response.length() == 0)
            throw new IllegalArgumentException("Server Response is Empty!");

        reservation.setToken(response.split(" ")[0]);
        if (response.split(" ").length > 1) {
            reservation.setAdultPrice(Integer.parseInt(response.split(" ")[1]));
            reservation.setChildPrice(Integer.parseInt(response.split(" ")[2]));
            reservation.setInfantPrice(Integer.parseInt(response.split(" ")[3]));
        }
        else {
            System.out.println("server did not send adult/child/infant price => set to 0");
            reservation.setAdultPrice(0);
            reservation.setChildPrice(0);
            reservation.setInfantPrice(0);
        }

        System.out.println("============== TOKEN AND PRICES SET");


//        System.out.println("========================== WRITTEN:\n" + resLog + '\n');

//        System.out.println("========= QUERY:");
//        System.out.println(query);

        // search for flight and initialize it
        boolean relatedFlightFound = false;
        ArrayList<Flight> relatedFlights = search(new SearchBean(reservation.getFlight().getOriginCode(),
                reservation.getFlight().getDestCode(), reservation.getFlight().getDate()));
        System.out.println("\n\n\n\n");
        for (Flight flight : relatedFlights)
            System.out.println(flight.toString() + '\n');
        System.out.println("\n\n\n\n");


        for (Flight flight : relatedFlights) {
            if (flight.getFlightNo().equals(reservation.getFlight().getFlightNo())) {
                for (Map.Entry<SeatClass, Integer> entry: flight.getSeats().entrySet()) {
                   /*   Price Query For Every ClassSeat   */
                    PriceBean priceBean = new PriceBean(flight.getOriginCode(),
                            flight.getDestCode(), flight.getAirlineCode(), entry.getKey().getClassOfSeat());
                    String[] prices = price(priceBean);


                    entry.getKey().setAdultPrice(Integer.parseInt(prices[0]));
                    entry.getKey().setChildPrice(Integer.parseInt(prices[1]));
                    entry.getKey().setInfantPrice(Integer.parseInt(prices[2]));

                    int adultPrice = entry.getKey().getAdultPrice();
                    int childPrice = entry.getKey().getChildPrice();
                    int infantPrice = entry.getKey().getInfantPrice();

                    entry.getKey().setAdultPrice(adultPrice);
                    entry.getKey().setChildPrice(childPrice);
                    entry.getKey().setInfantPrice(infantPrice);
                }

                reservation.setFlight(flight);
                System.out.println("========== FLIGHT FOR RES SET:");
                System.out.println(flight.toString());

                SeatClass selectedSeatClass = flight.getSeatClassByName(reservation.getSeatClass().getClassOfSeat());

                System.out.println("================= SELECTED: " + selectedSeatClass.toString());
                reservation.setSeatClass(selectedSeatClass);
                System.out.println("============= SEAT CLASS SET: ");
                System.out.println(selectedSeatClass.toString());

                relatedFlightFound = true;
                break;
            }
        }
        if (!relatedFlightFound)
            throw new IllegalArgumentException("ConnectionHandler: related flight not found!!!");

        String resLog = "RES " + reservation.getFlight().getFlightId() + " " + reservation.getToken() + " " +
                reservation.getAdultCount() + " " + reservation.getChildCount() + " " + reservation.getInfantCount();
        LOGGER.info(resLog);


        // sear for seat class and initialize it - setPrices()


        FinalizeBean finBean = finalize(response.split(" ")[0]);
        reservation.setRefCode(finBean.getRefCode());

        List<Ticket> tickets = new ArrayList<Ticket>();
        String finLog = "FINRES " + reservation.getToken() + " " + reservation.getId() + " " +
                reservation.getTotalPrice();
        LOGGER.info(finLog);
//        System.out.println("========================== WRITTEN:\n" + finLog + '\n');

        List<String> ticketNos = finBean.getTicketNumbers();
        String ticketLog = "";
        for (int i = 0; i < ticketNos.size(); i++) {
            Ticket ticket = reservation.getTickets().get(i);
            ticket.setTicketNo(ticketNos.get(i));

            ticketLog = "TICKET " + reservation.getRefCode() + " " + ticket.getTicketNo() + " " +
                    ticket.getPassenger().getSID() + " " + ticket.getPassenger().getAgeType() + " " +
                    (ticket.getPassenger().getAgeType().equals("adult") ? reservation.getSeatClass().getAdultPrice() :
                            ticket.getPassenger().getAgeType().equals("child") ? reservation.getSeatClass().getChildPrice() :
                                    reservation.getSeatClass().getInfantPrice());
        }
        LOGGER.info(ticketLog);
//        System.out.println("========================== WRITTEN:\n" + ticketLog + '\n');
    }

    public FinalizeBean finalize (String token) throws  IllegalArgumentException {
        String query = "FIN " + token;
        String response = serverConnection.getResponse(query);
        if (response.length() == 0)
            throw new IllegalArgumentException("Server Response is Empty!");

        String[] respLines = response.split("\n");

        FinalizeBean finBean = new FinalizeBean();
        finBean.setRefCode(respLines[0]);
        for (int i = 1; i < respLines.length; i++)
            finBean.addTicketNo(respLines[i]);

        return finBean;
    }

    public ArrayList<Flight> search(SearchBean searchBean) throws IllegalArgumentException {
        String origin = null, destination = null, airline = null, flightNo = null,
                date = null, depart = null, arrival = null, airplane = null;
        Map<SeatClass, Integer> seats = new HashMap<SeatClass, Integer>();
        ArrayList<Flight> flights = new ArrayList<Flight>();
        String query = "AV " + searchBean.getOrigin() + " "
                + searchBean.getDestination() + " " + searchBean.getDepart();

        String response = serverConnection.getResponse(query);
        if (response.length() == 0)
            throw new IllegalArgumentException("Server Response is Empty!");

        String responseSegments[] = response.split("\n");

        for (int i = 0; i < responseSegments.length; i++) {
            String segments[] = responseSegments[i].split(" ");
            seats.clear();
            if ((i % 2) == 0) {
                airline = segments[0];
                flightNo = segments[1];
                date = segments[2];
                origin = segments[3];
                destination = segments[4];
                depart = segments[5];
                arrival = segments[6];
                airplane = segments[7];
            } else {
                for (int j = 0; j < segments.length; j++) {
                    String clsName = String.valueOf(segments[j].charAt(0));
                    String seatsLeft = String.valueOf(segments[j].charAt(1));
                    if (seatsLeft.equals("A"))
                        seatsLeft = "10";
                    else if (seatsLeft.equals("C"))
                        seatsLeft = "0";
                    SeatClass seatClass = new SeatClass(origin, destination, airline, clsName);
                    seats.put(seatClass, Integer.valueOf(seatsLeft));
                }
                Flight flight = new Flight(origin, destination, airline, flightNo, date, depart, arrival, airplane, seats);
                flights.add(flight);
            }
        }
        return flights;
    }

    public String[] price(PriceBean priceBean) throws IllegalArgumentException {
        String query = "PRICE " + priceBean.getOrigin() + " " + priceBean.getDestination() + " "
                + priceBean.getAirline() + " " + priceBean.getSeatClass();
        String response = serverConnection.getResponse(query);
        if (response.length() == 0)
            throw new IllegalArgumentException("Server Response is Empty!");

        return response.split(" ");
    }
}