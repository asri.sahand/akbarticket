package service.ticketsmanagement;

import business.*;
import business.handlers.ReserveHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Moein Sorkhei on 4/21/2017.
 */

@Path("/ticketsmanagement")
public class TicketsManagement {

    @POST
    @Path("/gettickets")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTickets(ReserveInfo reserveInfo) {
        System.out.println("UMADDDDDD....");
        Flight flight = new Flight(reserveInfo.getOrigin(), reserveInfo.getDest(), reserveInfo.getAirlineCode(),
                reserveInfo.getFlightNo(), reserveInfo.getDate(), reserveInfo.getDepartTime(), reserveInfo.getArriveTime(),
                reserveInfo.getAirplaneModel(), null);

        SeatClass seatClass = new SeatClass(reserveInfo.getOrigin(), reserveInfo.getDest(), reserveInfo.getAirlineCode(),
                reserveInfo.getClassOfSeat(), -1, -1, -1);

        Reservation res = new Reservation(reserveInfo.getAdultCount(), reserveInfo.getChildCount(), reserveInfo.getInfantCount(),
                flight, seatClass);

//        List<String> names = reserveInfo.getNames();
//        List<String> snames = reserveInfo.getSnames();
//        List<String> sids = reserveInfo.getSids();
//        List

        List<PassengerInfo> passengers = reserveInfo.getPassengers();
        for (PassengerInfo passenger : passengers)
            res.addTicket(new Ticket("", new Passenger(passenger.getName(), passenger.getSname(),
                    passenger.getSid(), passenger.getGender(), passenger.getAgeType())));

        ReserveHandler handler = new ReserveHandler();
        System.out.println("just before reserve...");
        handler.reserve(res);
        System.out.println("sent reserve");
        System.out.println(res.getTickets());
        List<Ticket> tickets = res.getTickets();
//        boolean result = true;
        return  Response.status(200).entity(tickets).build();
    }
}
