package service.ticketsmanagement;

import java.util.List;

/**
 * Created by Moein Sorkhei on 4/21/2017.
 */
public class ReserveInfo {
    private String airlineCode;
    private String flightNo;
    private String date;
    private String origin;
    private String departTime;
    private String dest;
    private String arriveTime;
    private String airplaneModel;
    private String classOfSeat;
    private int adultCount, childCount, infantCount;
    private List<PassengerInfo> passengers;

//    public ReserveInfo(String airlineCode, String flightNo, String date, String origin, String departTime, String dest,
//                       String arriveTime, String airplaneModel, String classOfSeat, int adultCount, int childCount,
//                       int infantCount, List<Passenger> passengers) {
//        this.airlineCode = airlineCode;
//        this.flightNo = flightNo;
//        this.date = date;
//        this.origin = origin;
//        this.departTime = departTime;
//        this.dest = dest;
//        this.arriveTime = arriveTime;
//        this.airplaneModel = airplaneModel;
//        this.classOfSeat = classOfSeat;
//        this.adultCount = adultCount;
//        this.childCount = childCount;
//        this.infantCount = infantCount;
//        this.passengers = passengers;
//    }


    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public void setArriveTime(String arriveTime) {
        this.arriveTime = arriveTime;
    }

    public void setAirplaneModel(String airplaneModel) {
        this.airplaneModel = airplaneModel;
    }

    public void setClassOfSeat(String classOfSeat) {
        this.classOfSeat = classOfSeat;
    }

    public void setAdultCount(int adultCount) {
        this.adultCount = adultCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public void setInfantCount(int infantCount) {
        this.infantCount = infantCount;
    }

    public void setPassengers(List<PassengerInfo> passengers) {
        this.passengers = passengers;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public String getDate() {
        return date;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDepartTime() {
        return departTime;
    }

    public String getDest() {
        return dest;
    }

    public String getArriveTime() {
        return arriveTime;
    }

    public String getAirplaneModel() {
        return airplaneModel;
    }

    public String getClassOfSeat() {
        return classOfSeat;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public int getChildCount() {
        return childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public List<PassengerInfo> getPassengers() {
        return passengers;
    }
}