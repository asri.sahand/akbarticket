package service.ticketsmanagement;

/**
* Created by Moein Sorkhei on 4/21/2017.
*/
public class PassengerInfo {
    private String name;
    private String sname;
    private String sid;
    private String ageType;
    private String gender;

//    public PassengerInfo(String name, String sname, String sid, String gender, String ageType) {
//        this.name = name;
//        this.sname = sname;
//        this.sid = sid;
//        this.gender = gender;
//        this.ageType = ageType;
//    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public String getName() {
        return name;
    }

    public String getSname() {
        return sname;
    }

    public String getSid() {
        return sid;
    }

    public String getGender() {
        return gender;
    }

    public String getAgeType() {
        return ageType;
    }
}
