package repository;

import business.SeatClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class SeatClassRepo {
    private List<SeatClass> seatClasses;
    private static SeatClassRepo seatClassRepo = new SeatClassRepo();

    private SeatClassRepo() { seatClasses = new ArrayList<SeatClass>(); }

    public static SeatClassRepo getInstance() { return seatClassRepo; }

    public SeatClass getSeatClassById(int Id) {
        for (SeatClass seatClass : seatClasses)
            if (seatClass.getId() == Id)
                return seatClass;
        return null;
    }

    public void addToRepo(SeatClass seatClass) {
        seatClasses.add(seatClass);
        seatClass.setId(seatClasses.size());
    }

    public SeatClass getSeatClassIfExists(SeatClass seatClass) {
        for (SeatClass temp : seatClasses)
            if (temp.getOriginCode().equals(seatClass.getOriginCode()) &&
                    temp.getDestCode().equals(seatClass.getDestCode()) &&
                    temp.getAirlineCode().equals(seatClass.getAirlineCode()) &&
                    temp.getClassOfSeat().equals(seatClass.getClassOfSeat()))
                return temp;
        return null;
    }
}