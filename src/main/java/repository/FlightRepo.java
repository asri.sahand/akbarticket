package repository;

import business.Flight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */
public class FlightRepo {
    private List<Flight> flights;
    private static FlightRepo flightRepo = new FlightRepo();

    private FlightRepo() { flights = new ArrayList<Flight>(); }

    public static FlightRepo getInstance() { return flightRepo; }

    public Flight getFlightById(int flightId) {
        for (Flight flight : flights)
            if (flight.getFlightId() == flightId)
                return flight;
        return null;
    }

    public void addToRepo(Flight flight) {
        flights.add(flight);
        flight.setFlightId(flights.size());
    }

    public Flight getFlightIfExists(Flight flight) {
        for (Flight temp : flights)
            if (temp.getFlightNo().equals(flight.getFlightNo()) &&
                    temp.getOriginCode().equals(flight.getOriginCode()) &&
                    temp.getDestCode().equals(flight.getDestCode()) &&
                    temp.getDate().equals(flight.getDate()))
                return temp;
        return null;
    }
}