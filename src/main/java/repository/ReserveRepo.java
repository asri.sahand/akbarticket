package repository;

import business.Reservation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */

public class ReserveRepo {
    private List<Reservation> reservations;
    private static ReserveRepo reserveRepo = new ReserveRepo();

    private ReserveRepo() {
        reservations = new ArrayList<Reservation>();
    }

    public static ReserveRepo getInstance() {
        return reserveRepo;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public boolean reserveExists(Reservation reservation) {
        return reservations.contains(reservation);
    }

//    public void addReservation(Reservation reservation) {
//        reservations.add(reservation);
//    }

    public Reservation getResById(int Id) {
        for (Reservation reservation : reservations)
            if (reservation.getId() == Id)
                return reservation;
        return null;
    }

    public void addToRepo(Reservation reservation) {
        reservations.add(reservation);
        reservation.setId(reservations.size());
    }
}
