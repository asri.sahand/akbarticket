package controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */

public class PerformReserve extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        // after sending res info to server and getting response

//        int adultCount = 1;
//        int childCount = 1;
//        int infantCount = 1;
//        int adultPrice = 1000;
//        int childPrice = 1000;
//        int infantPrice = 1000;
////        SeatClass seatClass = null;
//        String token = "0";
//        String refCode = "0";
//        Flight flight = null;
//        List<Ticket> tickets = null;

//
//        int flightId = Integer.parseInt(request.getParameter("flight_id"));
//        Flight flight = FlightRepo.getInstance().getFlightById(flightId);
//
//        int seatClassId = Integer.parseInt(request.getParameter("seat_id"));
//        SeatClass seatClass = SeatClassRepo.getInstance().getSeatClassById(seatClassId);

        // reserve must be added to the repository by now!


        // for test
//        SeatClass seatClass = new SeatClass("THR", "MHD", "IR", "CLass A", 3000, 2000, 1000);
//        seatClass.setId(1);
//        SeatClassRepo.getInstance().addToRepo(seatClass);
//
//        Map<SeatClass, Integer> map = new HashMap<SeatClass, Integer>();
//        map.put(seatClass, 10);
//
//        Flight flight = new Flight("THR", "MHD", "IR", "1000", "05Feb", "17:40", "18:50", "A300",map);
//        flight.setFlightId(1);
//        FlightRepo.getInstance().addToRepo(flight);
//
//        Reservation reservation = new Reservation(1, 1, 1, flight, seatClass);
//        reservation.setId(1);
//        ReserveRepo.getInstance().addToRepo(reservation);




//        int resId = Integer.parseInt(request.getParameter("res_id"));
//        Reservation res = ReserveRepo.getInstance().getResById(resId);
//        Logger.writeLog("===================================");
//        System.out.println("===================================");
//        Logger.writeLog(res == null ? "RES IS NULL" : "RES IS NOT NULL");
//        System.out.println(res == null ? "RES IS NULL" : "RES IS NOT NULL");
//        Logger.writeLog("===================================");
//        System.out.println("===================================");

//        ReserveBean resBean = new ReserveBean(flight, seatClass);




        /*String[] names = request.getParameterValues("name");
        String[] snames = request.getParameterValues("sname");
        String[] sids = request.getParameterValues("sid");

        for (int i = 0; i < names.length; i++)
            resBean.addPassengers(new Passenger(names[i], snames[i], sids[i]));*/








//        request.setAttribute("reserve", res);
//        set attribute for passengers
//        request.setAttribute("");
        request.getRequestDispatcher("ReservePage.jsp").forward(request, response);

    }
}
