package controllers;

import business.*;
import business.handlers.ReserveHandler;
import general.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Moein Sorkhei on 3/9/2017.
 */

public class PerformFinalize extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String airlineCode = request.getParameter("airline_code");
        String flightNo = request.getParameter("flight_no");
        String date = request.getParameter("date");
        String origin = request.getParameter("origin");
        String departTime = request.getParameter("depart_time");
        String dest = request.getParameter("dest");
        String arriveTime = request.getParameter("arrive_time");
        String airplaneModel = request.getParameter("airplane_model");
        String classOfSeat = request.getParameter("seat_class");

        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("adult_count: " + request.getParameter("adult_count"));
        System.out.println("child_count: " + request.getParameter("child_count"));
        System.out.println("infant_count: " + request.getParameter("infant_count"));
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

        int adultCount = Integer.parseInt(request.getParameter("adult_count"));
        int childCount = Integer.parseInt(request.getParameter("child_count"));
        int infantCount = Integer.parseInt(request.getParameter("infant_count"));


        // REPOSITORY AFFAIRS
        Flight flight = new Flight(origin, dest, airlineCode, flightNo, date, departTime, arriveTime,
                airplaneModel, null);
        SeatClass seatClass = new SeatClass(origin, dest, airlineCode, classOfSeat, -1, -1, -1);
        Reservation res = new Reservation(adultCount, childCount, infantCount, flight, seatClass);
//        int resId = Integer.parseInt(request.getParameter("res_id"));
//        Reservation res = ReserveRepo.getInstance().getResById(resId);


        String[] names = request.getParameterValues("name");
        String[] snames = request.getParameterValues("sname");
        String[] sids = request.getParameterValues("sid");
        String[] ages = request.getParameterValues("age");
        String[] genders = request.getParameterValues("gen");

        if (!(Validator.areNamesValid(names) && Validator.areNamesValid(snames) && Validator.areSIDsValid(sids)))
            request.getRequestDispatcher("ErrorPage.jsp?errorMessage=INPUTS%20NOT%20VALID!!").forward(request, response);


        for (int i = 0; i < names.length; i++)
            res.addTicket(new Ticket("", new Passenger(names[i], snames[i], sids[i], genders[i], ages[i])));


        // handler
        System.out.println("======================================");
        System.out.println("BEFORE HANDLER");
        System.out.println("======================================");
        ReserveHandler handler = new ReserveHandler();
        handler.reserve(res);

        System.out.println("========================================");

//        System.out.println(res.toString());
        System.out.println("========================================");

        // dispatch to JSP
        request.setAttribute("reserve", res);
        request.getRequestDispatcher("TicketsPage.jsp").forward(request, response);
    }
}
