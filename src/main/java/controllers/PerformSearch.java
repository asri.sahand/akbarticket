package controllers;

import beans.PriceBean;
import beans.SearchBean;
import business.Flight;
import business.SeatClass;
import connection.ConnectionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by sehand on 3/10/2017.
 */
public class PerformSearch extends HttpServlet {

    private String origin;
    private String destination;
    private String depart;
    private String adultCount;
    private String childCount;
    private String infantCount;

    private int reserveNum = 0;
    private ArrayList<Flight> flights;

    private final String HOMEPAGE_ADDRESS_HYPER_LINK =
            "<ticketsmanagement href=\"http://localhost:8088/AkbarTicket/\">بازگشت</ticketsmanagement>";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        reserveNum = 0;
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        PrintWriter writer = response.getWriter();

        writer.println("<!DOCTYPE html>");
        writer.println("<html dir=\"rtl\" lang=\"fa\">");


        writer.println("<head>\n" +
                "\t<!-- <link rel=\"stylesheet\" type=\"text/css\" href=\"Reserve-Style.css\"> -->\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/../css/normalize.css\">\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/../css/FlightPage/header.css\">\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/../css/FlightPage/menu.css\">\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/../css/FlightPage/FlightPage.css\">\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/../css/FlightPage/list.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n" +
                "    <title>Flight Page</title>\n" +
                "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n" +
                "\t<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n" +
                "\t<script src=\"https://unpkg.com/babel-standalone@6/babel.min.js\"></script>\n" +
                "\t<script type=\"text/babel\" src=\"scripts/filter.js\"></script>\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1\">\n" +
                "</head>");

//        writer.println("<body>");


        try {
            setParams(request);
            SearchBean searchBean = new SearchBean(origin, destination, depart);
            ConnectionHandler connectionHandler = new ConnectionHandler();
            flights = connectionHandler.search(searchBean);

            for (int i = 0; i < flights.size(); i++) {
                Flight flight = flights.get(i);
                reserveNum += flight.getSeats().size();

                HashMap<SeatClass, Integer> seats = (HashMap<SeatClass, Integer>) flight.getSeats();
//                for (Map.Entry<SeatClass, Integer> entry: seats.entrySet()) {
                for(Iterator<Map.Entry<SeatClass, Integer>> it = seats.entrySet().iterator(); it.hasNext();){
                    Map.Entry<SeatClass, Integer> entry = it.next();

                    int capacity = entry.getValue();
                    if ((Integer.parseInt(adultCount) + Integer.parseInt(childCount) + Integer.parseInt(infantCount)) > capacity) {
                        reserveNum--;
//                       flight.getSeats().remove(entry.getKey());
                        it.remove();
                        continue;
                    }

                   /*   Price Query For Every ClassSeat   */
                    PriceBean priceBean = new PriceBean(flight.getOriginCode(),
                            flight.getDestCode(), flight.getAirlineCode(), entry.getKey().getClassOfSeat());
                    String[] prices = connectionHandler.price(priceBean);

                    entry.getKey().setAdultPrice(Integer.parseInt(prices[0]));
                    entry.getKey().setChildPrice(Integer.parseInt(prices[1]));
                    entry.getKey().setInfantPrice(Integer.parseInt(prices[2]));

                    int adultPrice = entry.getKey().getAdultPrice();
                    int childPrice = entry.getKey().getChildPrice();
                    int infantPrice = entry.getKey().getInfantPrice();
                }
            }

            writer.println("<body>\n" +
                    "\t<div class=\"row\" id=\"akbar-row\">\n" +
                    "            <div class=\"col-md-2 rtl\" ></div>\n" +
                    "            \n" +
                    "            <div class=\"col-md-4 rtl col-xs-6\" id=\"akbar-col\">\n" +
                    "\t\t\t\t<img id=\"logo\" alt=\"\" src=\"../assets/LogoBlack.png\">\n" +
                    "                <span id=\"akbar-text\" class=\"ak\">اکبر تیکت</span>\n" +
                    "\t\t\t\t<span id=\"registered\">\n" +
                    "                    <i class=\"fa fa-registered\" id=\"reg\" aria-hidden=\"true\"></i>\n" +
                    "                </span>\n" +
                    "\t\t    </div>\n" +
                    "            \n" +
                    "\t\t    <div class=\"col-md-4 col-xs-3 rtl digit parent\" id=\"user-col\">\n" +
                    "\t\t\t\t<span id=\"user-logo\" class=\"child\">\n" +
                    "                    <i class=\"fa fa-user fa-2x ak\" aria-hidden=\"true\"></i>\n" +
                    "                </span>\n" +
                    "                <div id=\"uname\" tabindex=\"0\" class=\"onclick-menu child\">\n" +
                    "                    <span class=\"onclick-menu-content\" id=\"dd\">\n" +
                    "                        <button id=\"dd-btn1\" class=\"dd-btn\" onclick=\"alert('click 1')\">بلیت های من</button>\n" +
                    "                        <button id=\"dd-btn2\" class=\"dd-btn\" onclick=\"alert('click 2')\">خروج</button>\n" +
                    "                    </span>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        \n" +
                    "            \n" +
                    "            <div class=\"col-md-2 rtl\"></div>\n" +
                    "    </div>\n" +
                    "    \n" +
                    "    <div class=\"container-fluid\" id=\"container\">\n" +
                    "\t\t<div class=\"row\" id=\"purple-strip\">\n" +
                    "\t\t\t<div class=\"col-md-3 rtl purple-col\"></div>\n" +
                    "            <div class=\"col-md-3 col-xs-12 rtl purple-col\">\n" +
                    "                <span class=\"purple-span\" id=\"flight-search\">جستجوی پرواز</span>\n" +
                    "\t\t\t\t<span class=\"purple-span purple-arrow\" id=\"left-arrow-1\">\n" +
                    "                    <i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i>\n" +
                    "                </span>\n" +
                    "\t\t\t\t<span class=\"purple-span white-color\">انتخاب پرواز</span>\n" +
                    "                <span class=\"purple-span purple-arrow white-color\" id=\"left-arrow-2\">\n" +
                    "\t\t\t\t    <i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i>\n" +
                    "                </span>\n" +
                    "\t\t\t</div>\n" +
                    "            \n" +
                    "            <div class=\"col-md-3 col-xs-12 rtl purple-col\"> \n" +
                    "\t\t\t\t<span class=\"purple-span\">ورود اطلاعات</span>\n" +
                    "\t\t\t\t<span class=\"purple-span purple-arrow\" id=\"left-arrow-3\">\n" +
                    "                    <i  class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i>\n" +
                    "                </span>\n" +
                    "\t\t\t\t<span class=\"purple-span\">دریافت بلیت</span>\n" +
                    "            </div>\n" +
                    "            <div class=\"col-md-3 rtl purple-col\"></div>\n" +
                    "\t\t</div>\n" +
                    "\n" +
                    "\t\t<div class=\"tb-7 row\" id=\"below-purple\">\n" +
                    "\t\t\t<div class=\"col-md-2 rtl\"></div>\n" +
                    "\n" +
                    "\t\t\t<select onchange=\"priceSort()\" id=\"price-selector\">\n" +
                    "\t\t\t\t<option>صعودی</option>\n" +
                    "\t\t\t\t<option>نزولی</option>\n" +
                    "\t\t\t</select>\n" +
                    "\n" +
                    "\t\t\t<select onchange=\"filter()\" id=\"seat-class-selector\">\n" +
                    "\t\t\t</select>\n" +
                    "\n" +
                    "\t\t\t<select onchange=\"filter()\" id=\"airline-selector\">\n" +
                    "\t\t\t</select>\n" +
                    "\n" +
                    "\t\t\t<div class=\"col-md-4 col-xs-12 rtl bold tb-7\">\n" +
                    "\t\t\t\t<i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>\n" +
                    "\t\t\t\t<ticketsmanagement class=\"back-to-homepage\" href=\"http://localhost:8088/AkbarTicket\">برگشت به جستجو</ticketsmanagement>\n" +
                    "\t\t\t</div>\n" +
                    "            \n" +
                    "            <div class=\"col-md-4 col-xs-12 rtl bold tb-7\">");



            writer.println("<span id=\"flights-info\"> " + reserveNum + " " + "پرواز با مشخصات دلخواه شما پیدا شد" + "</span>\n" +
                    "            </div>\n" +
                    "            <div class=\"col-md-2\"></div>\n" +
                    "\t\t</div>\n" +
                    "        \n" +
                    "        <div id=\"gray-plain\">");


            for (int i = 0; i < flights.size(); i++) {
                Flight flight = flights.get(i);
                int j = 0;
                for (Map.Entry<SeatClass, Integer> entry: flight.getSeats().entrySet()) {
                    int sum = Integer.parseInt(adultCount)*entry.getKey().getAdultPrice()
                            + Integer.parseInt(childCount)*entry.getKey().getChildPrice()
                            + Integer.parseInt(infantCount)*entry.getKey().getInfantPrice();
                    writer.println("<form class=\"white-plain\" action=\"PerformReserve.do\" method=\"post\">\n" +
                            "                <input type=\"text\" name=\"airline_code\" hidden=\"true\" value=\"" + flight.getAirlineCode() + "\">" +
                            "                <input type=\"text\" name=\"flight_no\" hidden=\"true\" value=\"" + flight.getFlightNo() + "\">" +
                            "                <input type=\"text\" name=\"origin\" hidden=\"true\" value=\"" + flight.getOriginCode() + "\">" +
                            "                <input type=\"text\" name=\"depart_time\" hidden=\"true\" value=\"" + flight.getDepartTime() + "\">" +
                            "                <input type=\"text\" name=\"dest\" hidden=\"true\" value=\"" + flight.getDestCode() + "\">" +
                            "                <input type=\"text\" name=\"arrive_time\" hidden=\"true\" value=\"" + flight.getArrivalTime() + "\">" +
                            "                <input type=\"text\" name=\"seat_class\" hidden=\"true\" value=\"" + entry.getKey().getClassOfSeat() + "\">" +
                            "                <input type=\"text\" name=\"airplane_model\" hidden=\"true\" value=\"" + flight.getAirplaneModel() + "\">" +
                            "                <input type=\"text\" name=\"adult_price\" hidden=\"true\" value=\"" + entry.getKey().getAdultPrice() + "\">" +
                            "                <input type=\"text\" name=\"adult_count\" hidden=\"true\" value=\"" + adultCount + "\">" +
                            "                <input type=\"text\" name=\"child_price\" hidden=\"true\" value=\"" + entry.getKey().getChildPrice() + "\">" +
                            "                <input type=\"text\" name=\"child_count\" hidden=\"true\" value=\"" + childCount + "\">" +
                            "                <input type=\"text\" name=\"infant_price\" hidden=\"true\" value=\"" + entry.getKey().getInfantPrice() + "\">" +
                            "                <input type=\"text\" name=\"capacity\" hidden=\"true\" value=\"" + entry.getValue() + "\">" +
                            "                <input type=\"text\" name=\"infant_count\" hidden=\"true\" value=\"" + infantCount + "\">" +
                            "                <input type=\"text\" name=\"date\" hidden=\"true\" value=\"" + flight.getDate() + "\">" +
                            "                <div class=\"white-plain-right col-md-3 col-xs-12 rtl wp1 tb-6\">\n" +
                            "                    <div class=\"airline big-bold\">\n" +
                            "                        <span class=\"airline-name\">" + flight.getAirlineCode() + "</span>\n" +
                            "                        <span class=\"airline-digit\">" + flight.getFlightNo() + "</span>\n" +
                            "                    </div>\n" +
                            "                    <div class=\"digit wp1-bottom\">\n" +
                            "                        <i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i>\n" +
                            "                        <span>" + flight.getDate() + "</span>\n" +
                            "                    </div>\n" +
                            "                </div>\n" +
                            "                \n" +
                            "                <div class=\"white-plain-center col-md-6 col-xs-12 rtl wp1-mdcol big-bold wp1 tb-7\" id=\"org-dst-div\" >\n" +
                            "                    <span class=\"mid digit\">" + flight.getOriginCode() + " " + flight.getDepartTime() + "</span>\n" +
                            "                    <i class=\"fa fa-angle-double-left fa-2x white-arrow purple-arrow\" aria-hidden=\"true\"></i>\n" +
                            "                    <span class=\"digit\">" + flight.getDestCode() + " " + flight.getArrivalTime() + "</span>\n" +
                            "                    <div class=\"flights-item-info\">\n" +
                            "                        <div id=\"plane-name\">\n" +
                            "                            <i class=\"fa fa-plane fa-2x\" aria-hidden=\"true\"></i>\n" +
                            "                            <span>" + flight.getAirplaneModel() + "</span>\n" +
                            "                        </div>\n" +
                            "\n" +
                            "                        <div id=\"seats-info\">\n" +
                            "                            <i class=\"fa fa-suitcase fa-2x\" aria-hidden=\"true\"></i>\n" +
                            "                            <span class=\"seat-class-name\">" + entry.getKey().getClassOfSeat() + "</span>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </div>\n" +
                            "\n" +
                            "                <button class=\"white-plain-left col-md-3 online-reserve\">\n" +
                            "                    <div class=\"reserve-cost\">\n" +
                            "                        <span class=\"reserve-cost-digit\">" + sum + "</span> <span>ریال</span>\n" +
                            "                    </div>\n" +
                            "\n" +
                            "                    <div class=\"online-reserve-div big-bold\">رزرو آنلاین</div>\n" +
                            "                </button>\n" +
                            "\n" +
                            "            </form>");
                    j++;
                }
            }

            writer.println("</div>\n" +
                    "    <div>\n" +
                    "        <footer>\n" +
                    "            <span>معین سرخه ای - سهند عصری | دانشکده فنی دانشگاه تهران، بهار 1396</span>\n" +
                    "        </footer>\n" +
                    "    </div>\n" +
                    "</body>");
        } catch (NumberFormatException e) {
//            writer.println("تعداد افراد درست وارد نشده!");
            writer.println("<div id = \"flights-info\" >تعداد مسافران درست وارد نشده!</div>");
            writer.println(HOMEPAGE_ADDRESS_HYPER_LINK);
        } catch (InvalidDateException e) {
//            writer.println("تاریخ رفت درست وارد نشده!");
            writer.println("<div id = \"flights-info\" >تاریخ رفت درست وارد نشده!</div>");
            writer.println(HOMEPAGE_ADDRESS_HYPER_LINK);
        }

        writer.println("</body>");
        writer.println("</html>");
    }

    private void setParams(HttpServletRequest request) throws NumberFormatException, InvalidDateException {
        origin = request.getParameter("origin");
        destination = request.getParameter("destination");
        depart = request.getParameter("depart");
        adultCount = request.getParameter("adult_num");
        childCount = request.getParameter("child_num");
        infantCount = request.getParameter("infant_num");

        Integer.parseInt(adultCount);
        Integer.parseInt(childCount);
        Integer.parseInt(infantCount);

        if (!depart.equals("05Feb") && !depart.equals("06Feb") && !depart.equals("07Feb")) {
            throw new InvalidDateException();
        }
    }

    class InvalidDateException extends Exception {}

}
