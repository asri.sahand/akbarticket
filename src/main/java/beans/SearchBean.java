package beans;

/**
 * Created by Sahand Asri on 10/4/2017.
 */
public class SearchBean {
    private String origin;
    private String destination;
    private String depart;

    public SearchBean(String origin, String destination, String depart) {
        this.origin = origin;
        this.destination = destination;
        this.depart = depart;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }
}