package beans;

/**
 * Created by Sahand Asri on 3/4/2017.
 */
public class PriceBean {
    private String origin;
    private String destination;
    private String airline;
    private String seatClass;

    public PriceBean(String origin, String destination, String airline, String seatClass) {
        this.origin = origin;
        this.destination = destination;
        this.airline = airline;
        this.seatClass = seatClass;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }
}
