package beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/6/2017.
 */
public class FinalizeBean {
    private String refCode;
    private List<String> ticketNumbers;

    public FinalizeBean() {
        this.refCode = null;
        this.ticketNumbers = new ArrayList<String>();
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public void addTicketNo(String ticketNo) {
        ticketNumbers.add(ticketNo);
    }

    public String getRefCode() {
        return refCode;
    }

    public List<String> getTicketNumbers() {
        return ticketNumbers;
    }
}
