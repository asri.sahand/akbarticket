package beans;

import business.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moein Sorkhei on 3/4/2017.
 */

public class ReserveBean {
    private int adultCount;
    private int childCount;
    private int infantCount;
    private Flight flight;
    private SeatClass seatClass;
    private List<Passenger> passengers;

//    public ReserveBean() {
//        this.flight = flight;
//        this.seatClass = seatClass;
//        this.passengers = passengers;
//    }

    public void setParameters(Reservation res) {
        adultCount = res.getAdultCount();
        childCount = res.getChildCount();
        infantCount = res.getInfantCount();
        flight = res.getFlight();
        seatClass = res.getSeatClass();
        passengers = new ArrayList<Passenger>();

        for (Ticket ticket : res.getTickets())
            passengers.add(ticket.getPassenger());
    }

//    public void setPassengers(List<Passenger> passengers) {
//        this.passengers = passengers;
//    }

    public void addPassenger(Passenger passenger) {
        passengers.add(passenger);
    }

    public int getAdultCount() {
        return adultCount;
    }

    public int getChildCount() {
        return childCount;
    }

    public int getInfantCount() {
        return infantCount;
    }

    public Flight getFlight() {
        return flight;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }
}