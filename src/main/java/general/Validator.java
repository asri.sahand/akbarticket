package general;

/**
 * Created by Moein Sorkhei on 3/11/2017.
 */
public class Validator {
    public static boolean areNamesValid(String[] names) {
        for (String name : names)
            if (isInvalidName(name))
                return false;
        return true;
    }

    public static boolean isInvalidName(String name) {
        if (name.length() == 0)
            return true;
        for (int i = 0; i < name.length(); i++)
            if (Character.isDigit(name.charAt(i)))
                return true;
        return false;
    }

    public static boolean areSIDsValid(String[] sids) {
        for (String sid : sids)
            if (isInvalidSID(sid))
                return false;
        return true;
    }

    public static boolean isInvalidSID(String sid) {
        if (sid.length() == 0)
            return true;
        for (int i = 0; i < sid.length(); i++)
            if (Character.isLetter(sid.charAt(i)))
                return true;
        return false;
    }


}
