package general;

/**
 * Created by Moein Sorkhei on 3/10/2017.
 */
public class FormatConverter {
    public static String convertAirlineCode(String code) throws IllegalArgumentException {
        String formattedStr;
        if (code.equals("IR"))
            formattedStr = "ایران ایر";
        else if (code.equals("W5"))
            formattedStr = "ماهان";
        else
            throw new IllegalArgumentException("Invalid Airline Code!!! " + code);
        return formattedStr;
    }

    public static String convertCity(String code) throws IllegalArgumentException {
        String formattedStr;
        if (code.equals("THR"))
            formattedStr = "تهران";
        else if (code.equals("MHD"))
            formattedStr = "مشهد";
        else if (code.equals("IFN"))
            formattedStr = "اصفهان";
        else
            throw new IllegalArgumentException("Invalid City Code!!! " + code);
        return formattedStr;
    }

    public static String convertAirplaneCode(String code) throws IllegalArgumentException {
        String formattedStr;
        if (code.equals("M80"))
            formattedStr = "بوئینگ MD83";
        else if (code.equals("351"))
            formattedStr = "ایرباس A320";
        else
            throw new IllegalArgumentException("Invalid Airplane Code!!! " + code);
        return formattedStr;
    }

    public static String convertSeatClass(String code) {
        return "کلاس اقتصادی";
    }

    public static String formatTime(String time) {
        return time.substring(0, 2) + ":" + time.substring(2, 4);
    }
}
