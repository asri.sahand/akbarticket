/**
 * Created by Moein Sorkhei on 4/25/2017.
 */

var app = angular.module('akbarApp', ['ngRoute']);
app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'home.html',
            controller : 'HomeController'
        })

        .when('/tickets', {
            templateUrl: 'phase%205/html/GetTicket.html',
            controller: 'TicketsController'
        });
    $locationProvider.html5Mode(true);
});

app.controller('HomeController', function ($scope) {
    console.log("HomeCtrl Called.......");
//    console.log("MainCtrl Called.......")
//    console.log("MainCtrl Called.......")
//    console.log("MainCtrl Called.......")
//    console.log("MainCtrl Called.......")
//    console.log("MainCtrl Called.......")
    $scope.message = "Home Page!!!";
});

app.controller('TicketsController', function ($scope) {
    console.log("TicketsController called...");
//    console.log("TicketsController called...");
//    console.log("TicketsController called...");
//    console.log("TicketsController called...");
//    console.log("TicketsController called...");
//    console.log("TicketsController called...");
    $scope.message = "Tickets Page!!!";
});
