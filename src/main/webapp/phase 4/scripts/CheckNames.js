/**
 * Created by Moein Sorkhei on 3/17/2017.
 */

class Validator {
    static validate(names, snames, sids) {
        if (names.length === 0)
            return false;
        for (var i = 0; i < names.length; i++) {
            if (!names[i].value.match(/^[a-zA-Z]{3,}$/)) {
                return false;
            }
            if (!snames[i].value.match(/^[a-zA-Z]{3,}$/)) {
                return false;
            }
            if (!sids[i].value.match(/^\d{10}$/)) {
                return false;
            }
        }
        return true;
    }
}

function validate() {
    var names = document.getElementsByName("name");
    var snames = document.getElementsByName("sname");
    var sids = document.getElementsByName("sid");
    var validator = new Validator();
    if (Validator.validate(names, snames, sids))
        document.getElementById("info_form").submit();
    else
        sweetAlert("متاسفانه", "فرمت داده های ورودی درست نیست!", "error");
}