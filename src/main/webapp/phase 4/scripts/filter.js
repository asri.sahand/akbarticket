/**
 * Created by sehand on 3/15/2017.
 */

window.onload = function WindowLoad(event) {
    priceSort();
    initSeatClassFilter();
    initAirlineFilter();
}

function initAirlineFilter() {
    var airlines = [];
    var airlineNameElements = document.getElementsByClassName("airline-name");
    var airlineDigitElements = document.getElementsByClassName("airline-digit");
    for(var i = 0; i < airlineNameElements.length; i++) {
        var airlineName = airlineNameElements[i].innerHTML;
        var airlineDigit = airlineDigitElements[i].innerHTML;
        if(!airlines.contains(airlineName + airlineDigit))
            airlines.push(airlineName + airlineDigit);
    }

    var airlineSelectorInner = "<option>همه</option>>";
    var airlineSelector = document.getElementById("airline-selector");
    var airline;
    for(var i = 0; i < airlines.length; i++) {
        airlineSelectorInner += "<option>" + airlines[i] + "</option>>";
    }
    airlineSelector.innerHTML = airlineSelectorInner;
}

function initSeatClassFilter() {
    var seats = [];
    var seatsElements = document.getElementsByClassName("seat-class-name");
    for(var i = 0; i < seatsElements.length; i++) {
        var seat = seatsElements[i].innerHTML;
        if(!seats.contains(seat))
            seats.push(seat);
    }

    var seatClassSelectorInner = "<option>همه</option>>";
    var seatClassSelector = document.getElementById("seat-class-selector");
    var seat;
    for(var i = 0; i < seats.length; i++) {
        seatClassSelectorInner += "<option>" + seats[i] + "</option>>";
    }
    seatClassSelector.innerHTML = seatClassSelectorInner;
}

function priceSort() {
    var costAscSort = function (x, y) {
        return x.getElementsByClassName("reserve-cost-digit")[0].innerHTML - y.getElementsByClassName("reserve-cost-digit")[0].innerHTML;
    };

    var costDescSort = function (x, y) {
        return y.getElementsByClassName("reserve-cost-digit")[0].innerHTML - x.getElementsByClassName("reserve-cost-digit")[0].innerHTML;
    };

    var forms = document.getElementsByTagName("form");
    forms = Array.prototype.slice.call(forms, 0);
    var priceSelector = document.getElementById("price-selector");

    if(priceSelector[priceSelector.selectedIndex].value == "صعودی")
        forms.sort(costAscSort);
    else {
        forms.sort(costDescSort);
    }

    var parent = document.getElementById("gray-plain");
    parent.innerHTML = "";
    for(var i = 0; i < forms.length; i++) {
        parent.appendChild(forms[i]);
    }
}

function seatClassFilter() {
    var forms = document.getElementsByTagName("form");
    forms = Array.prototype.slice.call(forms, 0);

    var selector = document.getElementById("seat-class-selector");
    var selectedSeatClass = selector[selector.selectedIndex].value;

    /*   no filter check   */
    if(selectedSeatClass == "همه") {
        showAllForms(forms);
        return;
    }
    
    var filteredSeats = [];
    for(var i = 0; i < forms.length; i++) {
        var seatClass = forms[i].getElementsByClassName("seat-class-name")[0].innerHTML;
        if(seatClass == selectedSeatClass) {
            filteredSeats.push(forms[i]);
        }
    }

    for(var i = 0; i < forms.length; i++) {
        var flag = 0;
        // forms[i].style.display = "block";
        for(var j = 0; j < filteredSeats.length; j++) {
            if(forms[i] == filteredSeats[j]) { flag = 1; break; }
        }
        if(flag == 0) { forms[i].style.display = "none"; }
    }

    var parent = document.getElementById("gray-plain");
    parent.innerHTML = "";
    for(var i = 0; i < forms.length; i++) {
        parent.appendChild(forms[i]);
    }

    updateFlightNum();
}

function showAllForms(forms) {
    for(var i = 0; i < forms.length; i++) {
        forms[i].style.display = "block";
    }
    updateFlightNum();
}

function airLineFilter() {
    var forms = document.getElementsByTagName("form");
    forms = Array.prototype.slice.call(forms, 0);

    var selector = document.getElementById("airline-selector");
    var selectedAirline = selector[selector.selectedIndex].value;

    /*   no filter check   */
    if(selectedAirline == "همه") {
        showAllForms(forms);
        seatClassFilter();
        return;
    }

    var filteredAirlines = [];
    for(var i = 0; i < forms.length; i++) {
        var airlineName = forms[i].getElementsByClassName("airline-name")[0].innerHTML;
        var airlineDigit = forms[i].getElementsByClassName("airline-digit")[0].innerHTML;
        if(airlineName + airlineDigit == selectedAirline) {
            filteredAirlines.push(forms[i]);
        }
    }

    // seatClassFilter();

    for(var i = 0; i < forms.length; i++) {
        var flag = 0;
        // forms[i].style.display = "block";
        for(var j = 0; j < filteredAirlines.length; j++) {
            if(forms[i] == filteredAirlines[j]) { flag = 1; break; }
        }
        if(flag == 0) { forms[i].style.display = "none"; }
    }

    var parent = document.getElementById("gray-plain");
    parent.innerHTML = "";
    for(var i = 0; i < forms.length; i++) {
        parent.appendChild(forms[i]);
    }

    updateFlightNum();
}

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

function filter() {
    // alert("changed!");
    var forms = document.getElementsByTagName("form");
    showAllForms(forms);
    seatClassFilter();
    airLineFilter();
}

function updateFlightNum() {
    var forms = document.getElementsByTagName("form");
    forms = Array.prototype.slice.call(forms, 0);

    var counter = 0;
    var i;
    for(i = 0; i < forms.length; i++)
        if(forms[i].style.display == "block")
            counter++;

    var flightInfo = document.getElementById("flights-info");
    flightInfo.innerHTML = counter + " پرواز با مشخصات دلخواه شما پیدا شد";
}

