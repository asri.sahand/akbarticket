/**
 * Created by Moein Sorkhei on 4/21/2017.
 */
(function () {
    var app = angular.module('finalize', [ ]);

    app.controller('FinalizeController', function ($http, $scope) {
        $scope.numOfPassengers = 1;
        $scope.reserveInfo = {};
        $scope.names = [];
        $scope.snames = [];
        $scope.SIDs = [];
        $scope.ages = [];
        $scope.genders = [];
        $scope.numOfFields = 5;

        $scope.getArrayWithSize = function (num) {
            return new Array(num);
        };

        $scope.finalize = function () {
//            $scope.passengers = [];
            $scope.passengers = [];

            for (var i = 0; i < $scope.numOfPassengers; i++) {
                $scope.passengers.push({
                    name: "a",
                    sname: "a",
                    sid: "1",
                    ageType: "a",
                    gender: "a"
                });
            }
            $scope.reserveInfo["airlineCode"] = "IR";
            $scope.reserveInfo["flightNo"] = "452";
            $scope.reserveInfo["date"] = "05Feb";
            $scope.reserveInfo["origin"] = "THR";
            $scope.reserveInfo["departTime"] = "1740";
            $scope.reserveInfo["dest"] = "MHD";
            $scope.reserveInfo["arriveTime"] = "1850";
            $scope.reserveInfo["airplaneModel"] = "M80";
            $scope.reserveInfo["classOfSeat"] = "Y";
            $scope.reserveInfo["adultCount"] = "1";
            $scope.reserveInfo["childCount"] = "0";
            $scope.reserveInfo["infantCount"] = "0";
            $scope.reserveInfo["passengers"] = $scope.passengers;

            console.log(JSON.stringify($scope.reserveInfo, null, "    "));
            $http.post('http://localhost:8080/akbar/ticketsmanagement/gettickets',
                $scope.reserveInfo).then(
                function(response) {
                    alert(JSON.stringify(response, null, "    "));
                }
            );
        };
    });
})();