/**
 * Created by Moein Sorkhei on 3/17/2017.
 */
var mins = 5;
var secondsRemaining = mins * 60;
var intervalHandle;

function tick(){
    var timeDisplay = document.getElementById("remaining_time_digit");
    var min = Math.floor(secondsRemaining / 60);
    var sec = secondsRemaining - (min * 60);
    if (sec < 10) {
        sec = "0" + sec;
    }
    timeDisplay.innerHTML = min.toString() + ":" + sec;

    if (secondsRemaining === 0){
//        sweetAlert("متاسفانه","زمان ورود اطلاعات به پایان رسیده است!", "info");
        swal({
                title: "متاسفانه",
                text: "زمان ورود اطلاعات به پایان رسیده است!",
                type: "warning"
            },
            function(){
                window.location = "../../";
            });
        clearInterval(intervalHandle);
    }
    secondsRemaining--;
}

function goToHome() {
    window.location = "../../";
}

function startCountdown(){
    intervalHandle = setInterval(tick, 1000);
}

window.onload = function() {
//    var timeDisplay = document.getElementById("remaining_time_digit");
//    timeDisplay.innerHTML = "";
    startCountdown();
    console.log("TIME STARTED");
};