/**
 * Created by Moein Sorkhei on 3/17/2017.
 */

function addUserInfoBlock(userType) {
//    var capacity = 4;
//    should be of type number
    var capacity = Number(document.getElementById("capacity").value);
    console.log("CAPACITY: " + capacity);
    if (!hasFreeSeats(capacity)) {
        sweetAlert("متاسفانه", "ظرفیت پر شده است!", "warning");
        return;
    }

    var userPrice = userType === 'adult' ? document.getElementById("ad_prc").innerText.split(" ")[0] :
            userType === 'child' ? document.getElementById("ch_prc").innerText.split(" ")[0] :
        document.getElementById("in_prc").innerText.split(" ")[0];

    var innerString = userType === 'adult' ? 'بزرگسال' : userType === 'child' ? 'خردسال' : 'نوزاد';

    var index = userType === 'adult' ? Number(document.getElementById("ad_cnt").innerText) + 1 :
            userType === 'child' ? (Number(document.getElementById("ad_cnt").innerText) +
        Number(document.getElementById("ch_cnt").innerText) + 1) :
        (Number(document.getElementById("ad_cnt").innerText) + Number(document.getElementById("ch_cnt").innerText) +
            Number(document.getElementById("in_cnt").innerText) + 1);

    var newNum = userType === 'adult' ? Number(document.getElementById("ad_cnt").innerText) + 1 :
            userType === 'child' ? Number(document.getElementById("ch_cnt").innerText) + 1 :
        Number(document.getElementById("in_cnt").innerText) + 1;

    var div = document.createElement('div');
    var tmp = userType === 'adult' ? 'AD' : userType === 'child' ? 'CH' : 'IN';
    div.className = "row inf added " + tmp;

    var innerInput = userType === 'adult' ? '<input type="hidden" name="age" value="adult">' : userType === 'child' ?
        '<input type="hidden" name="age" value="child">' : '<input type="hidden" name="age" value="infant">';

    div.innerHTML = ' <i class="fa fa-male info_item fa-lg" aria-hidden="true"></i>' +
        '<span>' + (index) + '-' + innerString + '</span> ' + innerInput +
        '<select name="gen" class="info_item" form="info_form">' + '<option value="male" class="info_item">آقای</option>' +
        '<option value="female">خانم</option></select>' +
        '<input name="name" form="info_form" class="info_item" type="text" placeholder="نام (انگلیسی)">' +
        '<input name="sname" form="info_form" class="info_item" type="text" placeholder="نام خانوادگی (انگلیسی)">' +
        '<input name="sid" form="info_form" class="info_item" type="text" placeholder="شماره ملی">';

    if (userType === 'adult') {
        console.log("IS ADULT");
        document.getElementById("ad_cnt").innerText = String(newNum);
        document.getElementById("ad_sm").innerText = (newNum * userPrice) + " " + "ریال" ;
        document.getElementById("adult_count").value = String(newNum);
        putInOrder(div, 'adult');
    }
    else if (userType === 'child') {
        console.log("IS CHILD");
        document.getElementById("ch_cnt").innerText = String(newNum);
        document.getElementById("ch_sm").innerText = (newNum * userPrice) + " " + "ریال";
        document.getElementById("child_count").value = String(newNum);
        putInOrder(div, 'child');
    }
    else {
        console.log("IS INF");
        document.getElementById("in_cnt").innerText = String(newNum);
        document.getElementById("in_sm").innerText = (newNum * userPrice) + " " + "ریال";
        document.getElementById("infant_count").value = String(newNum);
        putInOrder(div, 'infant');
    }
    var prevSum = Number(document.getElementById("sum").innerText.split(" ")[0]);
    document.getElementById("sum").innerText = prevSum + Number(userPrice) + ' ' + 'ریال' ;
}

function hasFreeSeats(capacity) {
    var numOfPeople = document.getElementsByClassName("row inf").length;
    return numOfPeople < capacity
}

function putInOrder(div, userType) {
    var adultsLen = document.getElementsByClassName("row inf AD").length;
    var childLen = document.getElementsByClassName("row inf CH").length;
    var infLen = document.getElementsByClassName("row inf IN").length;

    var refNode;
    if (userType === 'adult')
        refNode = document.getElementsByClassName("row inf")[adultsLen - 1];
    else if (userType === 'child')
        refNode = document.getElementsByClassName("row inf")[adultsLen + childLen - 1];
    else
        refNode = document.getElementsByClassName("row inf")[adultsLen + childLen + infLen - 1];

    refNode.parentNode.insertBefore(div, refNode.nextSibling);

//        updating following indices
    var childrenDivs = document.getElementsByClassName("row inf CH");
    var infDivs = document.getElementsByClassName("row inf IN");
    if (userType === 'adult' || userType === 'child') {
//            updating children indices
        if (userType === 'adult') {
            for (var indx = 0; indx < childrenDivs.length; indx++) {
                var span = childrenDivs[indx].getElementsByTagName("span")[0];
                var newIndex = Number(span.textContent.split("-")[0]) + 1;
                var innerStr = span.textContent.split("-")[1];
                span.textContent = newIndex + "-" + innerStr;
            }
        }

//          updating infants indices
        for (indx = 0; indx < infDivs.length; indx++) {
            span = infDivs[indx].getElementsByTagName("span")[0];
            newIndex = Number(span.textContent.split("-")[0]) + 1;
            innerStr = span.textContent.split("-")[1];
            span.textContent = newIndex + "-" + innerStr;
        }
    }
}